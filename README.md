# static.taevas.xyz

[static.taevas.xyz](https://static.taevas.xyz) is my collection of various static pages, those being either forks or fully original

It's not to be confused with [taevas.xyz](https://taevas.xyz), my personal website (not static), which has [a repository on GitHub](https://github.com/TTTaevas/taevas.xyz)
