# Error pages for taevas.xyz

Sometimes, the main website [taevas.xyz](https://taevas.xyz) can be unavailable, usually either because of a maintenance or because of poor code (unhandled error)

The pages in this folder, not listed on [static.taevas.xyz's main page](https://static.taevas.xyz), are what get rendered when such a thing happens

(if something goes wrong with the main website, this takes over; think of it as temporary redirection)
