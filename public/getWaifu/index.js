function waifu(route, params, display) {

	// Figure out what are the specified parameters
	let inputs = params.getElementsByTagName("input")
	let gif = inputs.gif.checked
	let nsfw = inputs.nsfw.checked

	// Create the request
	let xhr = new XMLHttpRequest()
	xhr.open("GET", `https://api.waifu.im/${route}?is_nsfw=${nsfw}&gif=${gif}`)
	xhr.setRequestHeader("Accept", "application/json")

	// Specify the behaviour upon receiving a response
	xhr.onreadystatechange = () => {
		if (xhr.readyState !== 4) return
		let data = JSON.parse(xhr.response).images[0]

		// Create the image element
		let image = document.createElement("img")
		image.setAttribute("src", data.url)
		image.setAttribute("alt", "Couldn't load the image... ><")

		// Put it on the website
		display.replaceChild(image, display.lastElementChild)
	}

	// Send the request
	xhr.send()
}
